%%
% DWE: this script calculates the eigenvalues of explicit, implicit-explicit and implicit schemes of the damped wave equation
% Copyright (C) 2020  Yury Alkhimenkov, Lyudmila Khakimova, Yury Podladchikov.
% Matlab R2018a

% This is a free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version. 

% Please cite us if you use our routine: 
% Alkhimenkov Y., Khakimova L., Podladchikov Y.Y., 2020. 
% Numerical stability of discrete schemes of Biot's poroelastic equations. 
% Geophysical Journal International.
%%
%% eigenvalues of the explicit scheme
close all,figure(4);clf; 
chi=0.0;
r_start = 0.00;
r_end   = 1.5;
fr = (r_end-r_start)/10000;
cou = 0;
dx = 1e-1; tau = 1;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'b-','linewidth',4,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'r-','linewidth',3,'MarkerSize',4);hold on;
cou = 0;dx = 1e+2;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'b--','linewidth',2.0,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'r--','linewidth',2.0,'MarkerSize',4);hold on;
xlabel('$$r\, (-)$$','interpreter','latex'); ylabel('$$\lambda _\xi \, (-)$$','interpreter','latex');
ylim([0 2]); %grid on;
legend('\lambda _1, DWE \chi=0, \Deltax = 10^{-1}','\lambda _2, DWE \chi=0, \Deltax = 10^{-1}',...
    '\lambda _1, DWE \chi=0, \Deltax = 10^{2}','\lambda _2, DWE \chi=0, \Deltax = 10^{2}', 'Location','northwest' );legend boxoff ;


%%
%% eigenvalues of the implicit-explicit scheme
figure(5);clf; 
chi=0.5;
r_start = 0.00;
r_end   = 1.5;
fr = (r_end-r_start)/10000;
cou = 0;
dx = 1e-1;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'b-','linewidth',4,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'r-','linewidth',3,'MarkerSize',4);hold on;
cou = 0;dx = 1e+2;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'b--','linewidth',2.0,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'r--','linewidth',2.0,'MarkerSize',4);hold on;
xlabel('$$r\, (-)$$','interpreter','latex'); ylabel('$$\lambda _\xi \, (-)$$','interpreter','latex');
ylim([0 2]); %grid on;
legend('\lambda _1, DWE \chi=0.5, \Deltax = 10^{-1}','\lambda _2, DWE \chi=0.5, \Deltax = 10^{-1}',...
    '\lambda _1, DWE \chi=0.5, \Deltax = 10^{2}','\lambda _2, DWE \chi=0.5, \Deltax = 10^{2}', 'Location','northwest' );legend boxoff ;



%%
%% eigenvalues of the implicit scheme
figure(6);clf; 
chi=1.0;
r_start = 0.00;
r_end   = 60.5;
fr = (r_end-r_start)/10000;
cou = 0;
dx = 1e-1;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'b-','linewidth',4,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'r-','linewidth',3,'MarkerSize',4);hold on;
cou = 0;dx = 1e+2;
for r = r_start :fr: r_end
    cou = cou +1 ;
%eig_1(cou) = (-4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) + 2)/(dx*r + 2);
%eig_2(cou) =-(4*r^2 + sqrt(dx^2*r^2 + 16*r^4 - 16*r^2) - 2)/(dx*r + 2);

%eig_1(cou) = (-4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) + 2*tau)/(dx*r + 2*tau);
%eig_2(cou) =-(4*r^2*tau + sqrt(16*r^4*tau^2 + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(dx*r + 2*tau);

eig_1(cou) = (2*chi*dx*r - 4*r^2*tau - dx*r + 2*tau + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2))/(2*(chi*dx*r + tau));
eig_2(cou) =-(-2*chi*dx*r + 4*r^2*tau + dx*r + sqrt(-16*chi*dx*r^3*tau + 16*r^4*tau^2 + 8*dx*r^3*tau + dx^2*r^2 - 16*r^2*tau^2) - 2*tau)/(2*(chi*dx*r + tau));
end
plot(r_start :fr: r_end,abs(eig_1),'m--','linewidth',2.0,'MarkerSize',6);hold on; 
plot(r_start :fr: r_end,abs(eig_2),'k--','linewidth',2.0,'MarkerSize',4);hold on;
xlabel('$$r\, (-)$$','interpreter','latex'); ylabel('$$\lambda _\xi \, (-)$$','interpreter','latex');
ylim([0 2]); xlim([0 60]);%grid on;
legend('\lambda _1, DWE \chi=1, \Deltax = 10^{-1}','\lambda _2, DWE \chi=1, \Deltax = 10^{-1}',...
    '\lambda _1, DWE \chi=1, \Deltax = 10^{2}','\lambda _2, DWE \chi=1, \Deltax = 10^{2}', 'Location','northwest' );legend boxoff ;
set(gca, 'XScale', 'log');
