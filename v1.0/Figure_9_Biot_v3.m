%%
% Biot_example 2: this script calculates the stability of explicit, implicit-explicit and implicit schemes of the Biot's equation
% Copyright (C) 2020  Yury Alkhimenkov, Lyudmila Khakimova, Yury Podladchikov.
% Matlab R2018a

% This is a free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version. 

% Please cite us if you use our routine: 
% Alkhimenkov Y., Khakimova L., Podladchikov Y.Y., 2020. 
% Numerical stability of discrete schemes of Biot's poroelastic equations. 
% Geophysical Journal International.
%%
close all,
figure(1)
clear,clf,colormap(jet), clc; format compact; format longG; 

% put these values into maple with different \chi 
% and you will get equations below
 A11 = 1;    A12 = 1/8;   A22 = 1/4;   % elast, eq.(40)
 R11 = 1;    R12 = 1/4;   R22 = 1/2;   % densit, eq.(40)
 
biot_0 = zeros(4,40);maxdis = 40;
chi = 1; tau=1;  
V=0.9773155191366;    %=V_1^{HF}
V2=0.327649776521625; %=V_2
for dis = 1:maxdis
    scale    = linspace(1,6.5,maxdis);
Lx           = 10.^(scale(dis)); 
nx           = 2001;
dx           = Lx/(nx-1);

% Biot, chi=0, positive root, explicit scheme for the Darcy's flux 
Z4 = 105; Z3 = 56*dx; Z2 = -68;  Z1 = -4*dx; Z0 = 4; % chi=0, eq.(58)
Disp_maple_analytic_m = [Z4 Z3 Z2 Z1 Z0];
biot_0(:,dis)  = dx*( 4.*roots(Disp_maple_analytic_m) );
% for dx=1, solution 40 gives 
%           -4.2919082266651
%           2.18489589962084
%          -1.03304479043873
%           1.00672378414965

% Biot, chi=1, positive root, implicit scheme for the Darcy's flux 
Z4_1 = 105; Z3_1 = -56*dx; Z2_1 = -68;  Z1_1 = 4*dx; Z0_1 = 4; % chi=1, eq.(63)
Disp_maple_analytic_m_1 = [Z4_1 Z3_1 Z2_1 Z1_1 Z0_1];
biot_1(:,dis)  = dx*( 4.*roots(Disp_maple_analytic_m_1) );
% for dx=1, solution 40 gives 
%           4.2919082266651
%          -2.18489589962084
%           1.03304479043873
%          -1.00672378414965; % note the same values but opposite signs in
%          lines 36-40 for chi=0

% Biot, chi=1/2, positive root, implicit-explicit scheme for the Darcy's flux
Biot_05(dis)       = dx./0.9773155191366; % eq.(62)
% for dx=1, it gives 1/0.9773155191366 = 1.023211010588

% Biot, chi=1/2,
Biot_05V2(dis)     = dx./V2;   % eq.(62) for V2 (see eq. (61), gamma_3)

% modified DWE, chi=0, positive root, explicit scheme for the Darcy's flux, eq.(54) 
DWE_0(dis)         = dx.*(1/V*sqrt( dx^2*1/V^2  +  16*tau^2 * (1/R22)^2 ) - 1/V^2*dx)/(4*tau *(1/R22));

% modified DWE, chi=1/2, positive root, explicit scheme for the Darcy's flux, eq.(A.11) with modifications from eq.(54)
DWE_05(dis)        = dx.*(1/V*sqrt(4*(1/2 - 1/2)^2*dx^2*1/V^2 + 16*tau^2 * (1/R22)^2) ...
    + 2*(1/2 - 1/2)*1/V^2*dx)/(4*tau *(1/R22)); 

% modified DWE, chi=1, positive root, explicit scheme for the Darcy's flux, eq.(A.11) with modifications from eq.(54)
DWE_1(dis)         = dx.*(1/V2*sqrt(4*(chi - 1/2)^2*dx^2*1/V2^2 + 16*tau^2 * (1/R22)^2) ...
    + 2*(chi - 1/2)*1/V2^2*dx)/(4*tau *(1/R22)); 

% eq.(A.6) or eq.(23) with modifications from eq.(54) 
par_lim            = 2 *tau *(1/R22) /  (1) ;

% Diffusion equation, eq.(12) with D from eq.(55)
dt_dif_stand(dis)  = dx*dx/tau / (2)  / (A22*(1- A12*A12/A22/A11)); 
end
x_ax = (10.^scale)/(nx-1);Lx1  = x_ax(1);Lx2 = x_ax(end);

figure(1);clf
% if you cannot see figure9, you may have different numbering of the roots of biot_0,
% biot_1. Try to plot them all (4 roots) and find the correct one. Some of
% them are negative, we do not need them. Example to plot other roots:
%plot(x_ax,biot_0(1,:),'o-','LineWidth',14); hold on 
%plot(x_ax,biot_0(2,:),'o-','LineWidth',14); hold on
%plot(x_ax,biot_0(3,:),'o-','LineWidth',14); hold on

plot(x_ax,biot_0(4,:),'ro-','LineWidth',7,'MarkerSize',12); hold on
plot(x_ax,Biot_05,'ko-','LineWidth',7,'MarkerSize',12); hold on

root2          = biot_1(2,:);root3          = biot_1(3,:); 
root2(root2<0) = root3(root3>0); % matlab jumps from posotive root to negative
plot(x_ax,root2,'x-','LineWidth',3, 'color','[0.95, 0.95, 0.06]','MarkerSize',13); hold on

plot(x_ax,biot_1(1,:),'bo-','LineWidth',5); hold on
plot(x_ax,Biot_05V2(1,:),'kx--','LineWidth',4); hold on % new lyamda 3
plot(x_ax,DWE_0,'k-.','LineWidth',3); hold on
plot(x_ax,DWE_05,'r-.','LineWidth',3.5); hold on
plot(x_ax,DWE_1,'g-.','LineWidth',3); hold on
plot(x_ax, dt_dif_stand,'--','color','[0.8500, 0.3250, 0.0980]','LineWidth',2.5);hold on%
plot([Lx1  Lx2 ], [par_lim par_lim],'--','color','[0., 0.8, 0.8]','LineWidth',2.5);hold on

set(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
legend('Biot Eq., \lambda_1, \chi=0','Biot Eq., \lambda_1, \chi=1/2','Biot Eq., \lambda_1, \chi=1',...
    'Biot Eq., \lambda_3, \chi=1','Biot Eq., \lambda_3, \chi=1/2','mDWE, \chi=0','mDWE, \chi=1/2','mDWE, \chi=1','Diffusion Eq.','Parabolic limit', 'Location','northwest');
xlabel('$$\Delta {x} \, (-)$$','interpreter','latex'); ylabel('$$\Delta {t} \, (-)$$','interpreter','latex');

ylim([10^(-2) 10^3]);    
xlim([10^(-2) 10^3]); legend boxoff ;

%%
if 1>4, return, end  % use 5 instead of 1 to stop here
%%

scLx        = 2000;
Lx          = 1*scLx;
nx          = 2001;
dx          = Lx/(nx-1);
x           = (-Lx/2:dx:Lx/2)';
% initial conditions
Vx          = zeros(nx+1,1);
Qx          = zeros(nx+1,1);
lamx        = 0.02*scLx;
stress_xx   = 0*exp(-(x/lamx).^2);
Prf         = 1*exp(-(x/lamx).^2);
% parameters
etaf_k      = 1; % \eta / k, see Biot's eq.
iM_ELan     = [A11 A12; A12   A22];
iMdvp       = [R11 R12; R12   R22];


%% This block caluclates numerically "r" (which is dt=dx*r) and creates a figure. 
%% So, for the scheme with chi=0.5, 1/r should be equal to V1_HF. See eq.(6)
cou = 0;val=0;
r_start = 0.9;
r_end   = 1.04; 
fr = (r_end-r_start)/10000; rsave05=0;
for r = r_start :fr: r_end
cou = cou +1 ;
chi = 0.0;
cg0 = chi; cg2=dx; cg4=r; cg6=etaf_k; cg8(11) = R11;cg8(12) = R12;cg8(22) = R22; cg10(11)=A11;cg10(12)=A12;cg10(22)=A22;
Gnew0 = [(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(11) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(11) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + cg0 * cg2 * cg4 * cg8(22) + cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(11) + 2*i * cg4 * cg6 * cg10(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(11) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); -(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(12) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(12) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(22) - cg0 * cg2 * cg4 * cg8(22) - cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(12) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg4 * cg6 * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(11) * cg8(22) + -2*i * cg0 * cg2 * (cg4 ^ 2) * (cg8(12) ^ 2) + 2*i * cg4 * cg6 * cg8(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 1 cg2 * cg8(12) * cg4 / (cg0 * cg2 * cg4 * cg8(22) + cg6); -2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -2*i / (cg0 * cg2 * cg4 * cg8(22) + cg6) * cg4 * cg6 * cg8(22) 0 -1 / (cg0 * cg2 * cg4 * cg8(22) + cg6) * (-cg0 * cg2 * cg4 * cg8(22) + cg2 * cg4 * cg8(22) - cg6);];
Gnew_ans0  = eig(Gnew0);
Gnew_ansA0(cou) = max(abs(Gnew_ans0(:)));

chi = 0.5;
cg0 = chi; cg2=dx; cg4=r; cg6=etaf_k; cg8(11) = R11;cg8(12) = R12;cg8(22) = R22; cg10(11)=A11;cg10(12)=A12;cg10(22)=A22;
Gnew05 = [(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(11) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(11) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + cg0 * cg2 * cg4 * cg8(22) + cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(11) + 2*i * cg4 * cg6 * cg10(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(11) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); -(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(12) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(12) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(22) - cg0 * cg2 * cg4 * cg8(22) - cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(12) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg4 * cg6 * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(11) * cg8(22) + -2*i * cg0 * cg2 * (cg4 ^ 2) * (cg8(12) ^ 2) + 2*i * cg4 * cg6 * cg8(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 1 cg2 * cg8(12) * cg4 / (cg0 * cg2 * cg4 * cg8(22) + cg6); -2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -2*i / (cg0 * cg2 * cg4 * cg8(22) + cg6) * cg4 * cg6 * cg8(22) 0 -1 / (cg0 * cg2 * cg4 * cg8(22) + cg6) * (-cg0 * cg2 * cg4 * cg8(22) + cg2 * cg4 * cg8(22) - cg6);];
Gnew_ans05  = eig(Gnew05);
Gnew_ansA05(cou) = max(abs(Gnew_ans05(:)));
if abs(Gnew_ansA05(cou)-1)<0.001, val=val+1; rsave05(val)=r;  end

chi = 1.0;
cg0 = chi; cg2=dx; cg4=r; cg6=etaf_k; cg8(11) = R11;cg8(12) = R12;cg8(22) = R22; cg10(11)=A11;cg10(12)=A12;cg10(22)=A22;
Gnew1 = [(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(11) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(11) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + cg0 * cg2 * cg4 * cg8(22) + cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(11) + 2*i * cg4 * cg6 * cg10(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(11) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); -(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(12) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(12) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(22) - cg0 * cg2 * cg4 * cg8(22) - cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(12) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg4 * cg6 * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(11) * cg8(22) + -2*i * cg0 * cg2 * (cg4 ^ 2) * (cg8(12) ^ 2) + 2*i * cg4 * cg6 * cg8(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 1 cg2 * cg8(12) * cg4 / (cg0 * cg2 * cg4 * cg8(22) + cg6); -2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -2*i / (cg0 * cg2 * cg4 * cg8(22) + cg6) * cg4 * cg6 * cg8(22) 0 -1 / (cg0 * cg2 * cg4 * cg8(22) + cg6) * (-cg0 * cg2 * cg4 * cg8(22) + cg2 * cg4 * cg8(22) - cg6);];
Gnew_ans1  = eig(Gnew1);
Gnew_ansA1(cou) = max(abs(Gnew_ans1(:)));
end
figure(6),clf

plot(r_start :fr: r_end,abs(Gnew_ansA0)-1*0,'g>-','linewidth',4,'MarkerSize',12); hold on;
plot(r_start :fr: r_end,abs(Gnew_ansA05)-1*0,'r>-','linewidth',4,'MarkerSize',12); hold on;
plot(r_start :fr: r_end,abs(Gnew_ansA1)-1*0,'bs-','linewidth',4,'MarkerSize',12); hold on;

legend('\chi=0','\chi=0.5','\chi=1.0', 'Location','northwest');
xlabel('$$r \, (-)$$','interpreter','latex'); ylabel('$$ Max (\gamma) \, (-)$$','interpreter','latex');
title('The difference between the three schemes, example 2')

%% This block caluclates numerically "r", which is dt=dx*r. So, for the scheme with chi=0.5, r should be equal to 1/V1_HF. See eq.(6) in the article
% This function (eigenvalues_general) is valid for any set of parameters,
% not just for example 2. "eigenvalues_general" is from Maple (copy)
% Note, that there is a numerical error in "eig" calculation around 1e-5.
% For example 2, this calculation shoul be equal
% to the roots in lines 34-57 of this script

chi     = 0.0; % use here 0(explicit) or 0.5 or 1(implicit) (or something between)

cg8(11) = R11;cg8(12) = R12;cg8(22) = R22; % material parameters
cg10(11)=A11; cg10(12)=A12; cg10(22)=A22;  % material parameters
cg0     = chi; cg2    =dx;  cg6=etaf_k;    % parameters
options = optimset('TolX',1e-10,'MaxIter',5000);
eigenvalues_general = fminbnd(@(cg4) (max(abs(eig(  [(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(11) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(11) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + cg0 * cg2 * cg4 * cg8(22) + cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(11) + 2*i * cg4 * cg6 * cg10(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(11) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); -(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(12) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(12) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(22) - cg0 * cg2 * cg4 * cg8(22) - cg6) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(12) + -2*i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*i * cg4 * cg6 * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); (2*i * cg0 * cg2 * (cg4 ^ 2) * cg8(11) * cg8(22) + -2*i * cg0 * cg2 * (cg4 ^ 2) * (cg8(12) ^ 2) + 2*i * cg4 * cg6 * cg8(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 1 cg2 * cg8(12) * cg4 / (cg0 * cg2 * cg4 * cg8(22) + cg6); -2*i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -2*i / (cg0 * cg2 * cg4 * cg8(22) + cg6) * cg4 * cg6 * cg8(22) 0 -1 / (cg0 * cg2 * cg4 * cg8(22) + cg6) * (-cg0 * cg2 * cg4 * cg8(22) + cg2 * cg4 * cg8(22) - cg6);] )))-1),0.1,1.2,options);

% lines 186-198 are approx. the same as line 183
% eigenvalues_general = fminbnd(@(cg4) (max(abs(eig(  [(-4 * cg0 * cg2 * cg4 ^ 3 * cg8(11) * cg8(22) * cg10(11) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(11) -...
%     4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + cg0 * cg2 * cg4 * cg8(22) + cg6) / (cg0 * cg2 * cg4 * cg8(22) +...
%     cg6) (-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(11) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*1i * cg0 * cg2 * (cg4 ^ 2) *...
%     cg8(22) * cg10(11) + 2*1i * cg4 * cg6 * cg10(11)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) (2*1i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*1i * cg2 * (cg4 ^ 2) *...
%     cg8(12) * cg10(11) + -2*1i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*1i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6); -(-4 * cg0 * cg2 * cg4 ^ 3 *...
%     cg8(11) * cg8(22) * cg10(12) + 4 * cg0 * cg2 * cg4 ^ 3 * cg8(12) ^ 2 * cg10(12) - 4 * cg4 ^ 2 * cg6 * cg8(11) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(22)) /...
%     (cg0 * cg2 * cg4 * cg8(22) + cg6) -(-4 * cg4 ^ 2 * cg6 * cg8(12) * cg10(12) + 4 * cg4 ^ 2 * cg6 * cg8(22) * cg10(22) - cg0 * cg2 * cg4 * cg8(22) - cg6) / (cg0 * cg2 *...
%     cg4 * cg8(22) + cg6) -(2*1i * cg0 * cg2 * (cg4 ^ 2) * cg8(22) * cg10(12) + 2*1i * cg4 * cg6 * cg10(12)) / (cg0 * cg2 * cg4 * cg8(22) + cg6) -(2*1i * cg0 * cg2 * (cg4 ^ 2) *...
%     cg8(22) * cg10(22) + 2*1i * cg2 * (cg4 ^ 2) * cg8(12) * cg10(12) + -2*1i * cg2 * (cg4 ^ 2) * cg8(22) * cg10(22) + 2*1i * cg4 * cg6 * cg10(22)) / (cg0 * cg2 * cg4 * cg8(22) +...
%     cg6); (2*1i * cg0 * cg2 * (cg4 ^ 2) * cg8(11) * cg8(22) + -2*1i * cg0 * cg2 * (cg4 ^ 2) * (cg8(12) ^ 2) + 2*1i * cg4 * cg6 * cg8(11)) / (cg0 * cg2 * cg4 * cg8(22) +...
%     cg6) 2*1i * cg4 * cg6 * cg8(12) / (cg0 * cg2 * cg4 * cg8(22) + cg6) 1 cg2 * cg8(12) * cg4 / (cg0 * cg2 * cg4 * cg8(22) + cg6); -2*1i * cg4 * cg6 * cg8(12) / (cg0 * cg2 *...
%     cg4 * cg8(22) + cg6) -2*1i / (cg0 * cg2 * cg4 * cg8(22) + cg6) * cg4 * cg6 * cg8(22) 0 -1 / (cg0 * cg2 * cg4 * cg8(22) + cg6) * (-cg0 * cg2 * cg4 * cg8(22) + cg2 * cg4 *...
%     cg8(22) - cg6);] )))-1),0.5,1.2,options);
Print_r = eigenvalues_general

% Biot, chi=0, positive root, explicit scheme for the Darcy's flux
%  (exact solution from maple)
Z4 = 105; Z3 = 56*dx; Z2 = -68;  Z1 = -4*dx; Z0 = 4; % chi=0, eq.(58)
Disp_maple_analytic_m = [Z4 Z3 Z2 Z1 Z0];
biot_0_validation  = ( 4.*roots(Disp_maple_analytic_m) );
print_r_from_V1_HF = biot_0_validation(4) % it should be equal(approx) to "Print_r", line 199

%% the code to test dt
figure(2);
 
% numerics
nout        = 250;
nt          = 7000;
chi         = 0.5;

A3_m        = (R11 * R22 - R12 ^ 2) * (A11 * A22 - A12 ^ 2); % eq. (34)*(33)
A2_m        = (A11 * R11 - 2 * A12 * R12 + A22 * R22) ;      % eq. (35)
V1_HF       = 1./ (( A2_m - (A2_m.*A2_m - 4.*A3_m)^0.5 )./2./A3_m ).^0.5; % eq.(31)
V2_HF       = 1./ (( A2_m + (A2_m.*A2_m - 4.*A3_m)^0.5 )./2./A3_m ).^0.5; % eq.(32)

CFL         = 0.999;
dt          = dx/ V1_HF  *  CFL;   % stable for chi=0.5
%dt          = dx/ V1_HF  *  1.00001; % (uncomment) not stable for chi=0.5 

print_r_from_V1_HF = 1/V1_HF  % this gives "r" for the scheme with \chi=0.5
rsave05_print = rsave05(round(end/2))  %this is "r". For \chi=0.5 should be equal to 1/V1_HF, see line 225, "print_r_from_V1_HF"

%% the code

for it = 1:nt  
   
    stress_xx     = stress_xx   + ( iM_ELan(1,1).*  diff(Vx,1,1)/dx + iM_ELan(1,2).*diff(Qx,1,1)/dx).*dt;
    Prf           = Prf         - ( iM_ELan(2,1).*  diff(Vx,1,1)/dx  + iM_ELan(2,2).*diff(Qx,1,1)/dx)*dt;
    Qx_old        = Qx; 
    Qx(2:end-1)   = (Qx_old(2:end-1)/dt -  diff(stress_xx,1,1)/dx.*iMdvp(2,1) - (diff(Prf,1,1)/dx +                     (1-chi).*Qx_old(2:end-1) .*etaf_k).*iMdvp(2,2))./(1/dt + chi.*iMdvp(2,2).*etaf_k);       
    Vx(2:end-1)   = (Vx(2:end-1)/dt     +  diff(stress_xx,1,1)/dx.*iMdvp(1,1) + (diff(Prf,1,1)/dx + (chi.*Qx(2:end-1) + (1-chi).*Qx_old(2:end-1)).*etaf_k).*iMdvp(1,2))*dt;      
   
    if mod(it,nout)==1 
        figure(2);
        subplot(4,1,1), plot(x,(Vx(2:end) + Vx(1:end-1))/2),    title('Vx'),
        subplot(4,1,2), plot(x,stress_xx),        title('Stress_{xx}'),
        subplot(4,1,3), plot(x,(Qx(2:end) + Qx(1:end-1))/2),title('Qxf'), 
        subplot(4,1,4), plot(x,Prf),title('Prf'), 
        xlabel(['it = ', num2str(it)]),drawnow
    end
end
