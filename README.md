---
 BiotCFL README | 03.12.2020. version 1.0
---

BiotCFL: these scripts (Matlab and Maple) calculate the stability of explicit, implicit-explicit and 
implicit schemes of the Biot's equations and the damped linear wave equation

Copyright (C) 2020  Yury Alkhimenkov, Lyudmila Khakimova, Yury Podladchikov.

Matlab R2018a
Maple 2019

BiotCFL is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BiotCFL is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BiotCFL.  If not, see <http://www.gnu.org/licenses/>.

---

Please cite us if you use our routine: 
Alkhimenkov Y., Khakimova L., Podladchikov Y.Y., 2020. 
Numerical stability of discrete schemes of Biot's poroelastic equations. 
Geophysical Journal International.
