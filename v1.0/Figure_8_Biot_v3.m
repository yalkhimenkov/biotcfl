%%
% Biot_example 1: this script calculates the stability of explicit, implicit-explicit and implicit schemes of the Biot's equation
% Copyright (C) 2020  Yury Alkhimenkov, Lyudmila Khakimova, Yury Podladchikov.
% Matlab R2018a

% This is a free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version. 

% Please cite us if you use our routine: 
% Alkhimenkov Y., Khakimova L., Podladchikov Y.Y., 2020. 
% Numerical stability of discrete schemes of Biot's poroelastic equations. 
% Geophysical Journal International.
%%
close all,figure(1)
clear,clf,colormap(jet), clc; format compact; format longG; 

% put these values into maple with different \chi 
% - will get equations below
 A11 = 1;    A12 = 1/2;   A22 = 1/1; % elast, eq.(40)
 R11 = 1;    R12 = 1/2;   R22 = 1;   % densit, eq.(40)

rt = zeros(4,40);maxdis = 40;
tau=1;  V=0.866025404068877; % V = 1/1.154700538 
for dis = 1:maxdis
    scale    = linspace(1,6.5,maxdis);
Lx           = 10.^(scale(dis)); 
nx           = 2001;
dx           = Lx/(nx-1);

% Biot, chi=0, positive root, explicit scheme for the Darcy's flux 
Biot_0(dis)  = dx.*(-0.3333333333*dx + 0.3333333333*sqrt(dx^2 + 12.) ); % eq.(45)

% Biot, chi=1, positive root, implicit scheme for the Darcy's flux 
Biot_1(dis)  = dx.*(+0.3333333333*dx + 0.3333333333*sqrt(dx^2 + 12.) ); % eq.(52)

% Biot, chi=1/2, positive root, implicit-explicit scheme for the Darcy's flux 
Biot_05(dis)  = dx.*1.154700538;  % eq.(49); V = 1/1.154700538 = 0.866025404068877

% DWE, chi=1, positive root, (A.11) with modifications from eq.(54)
chi = 1; 
DWE_1(dis)    = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2 * (1/R22)^2) ...
    + 2*(chi - 1/2)*1/V^2*dx)/(4*tau *(1/R22)); %  !!!  !!!

% DWE, chi=1/2 (put it manually), positive root, (A.11) with modifications from eq.(54)
DWE_05(dis)   = dx.*(1/V*sqrt(4*(1/2 - 1/2)^2*dx^2*1/V^2 + 16*tau^2 * (1/R22)^2) ...
    + 2*(1/2 - 1/2)*1/V^2*dx)/(4*tau *(1/R22)); % eq.(52)

% DWE, chi=0, positive root, explicit scheme for the Darcy's flux 
DWE_0(dis)    = dx.*(1/V*sqrt( dx^2*1/V^2  +  16*tau^2 * (1/R22)^2 ) - 1/V^2*dx)/(4*tau *(1/R22)); % eq.(54)

% eq.(A.6) or eq.(23) with modifications from eq.(54) 
par_lim       = 2 *tau *(1/R22) /  (1) ; %  !!!  !!!

% Diffusion equation, eq.(12) with D from eq.(55)
diff_eq(dis)  = dx*dx/tau / (2)  / (A22*(1- A12*A12/A22/A11)); 
end
rhoin = [1, 1/2; 1/2 , 1]; forDensity = inv(rhoin);
x_ax  = (10.^scale)/(nx-1);
Lx1   = x_ax(1);Lx2 = x_ax(end);

plot(x_ax,Biot_0,'ro-','LineWidth',7,'MarkerSize',12); hold on
plot(x_ax,Biot_05,'ko-','LineWidth',7,'MarkerSize',12); hold on
plot(x_ax,Biot_1,'bo-','LineWidth',5); hold on
plot(x_ax,DWE_0,'k-.','LineWidth',2); hold on
plot(x_ax,DWE_05,'r-.','LineWidth',2); hold on
plot(x_ax,DWE_1,'g-.','LineWidth',2); hold on
plot(x_ax, diff_eq,'--','color','[0.8500, 0.3250, 0.0980]','LineWidth',2.5);hold on%
plot([Lx1  Lx2 ], [par_lim par_lim],'--','color','[0., 0.8, 0.8]','LineWidth',2.5);hold on

set(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
legend('Biot Eq., \lambda_3, \chi=0','Biot Eq., \lambda_1, \chi=0; 1/2; 1',...
    'Biot Eq., \lambda_3, \chi=1','DWE, \chi=0','DWE, \chi=1/2','DWE, \chi=1','Diffusion Eq.','Parabolic limit', 'Location','northwest');
xlabel('$$\Delta {x} \, (-)$$','interpreter','latex'); ylabel('$$\Delta {t} \, (-)$$','interpreter','latex');
ylim([10^(-2) 10^3]);    
xlim([10^(-2) 10^3]); legend boxoff ;

%%
if 1>4, return, end  % use 5 instead of 1 to stop here
%%

figure(2);
scLx        = 2000;
Lx          = 1*scLx *2;
nx          = 2001;
dx          = Lx/(nx-1);
x           = (-Lx/2:dx:Lx/2)';
% initial conditions
Vx          = zeros(nx+1,1);
Qx          = zeros(nx+1,1);
lamx        = 0.02*Lx;
stress_xx   = 0*exp(-(x/lamx).^2);
Prf         = 1*exp(-(x/lamx).^2);
% parameters
etaf_k      = 1;
iM_ELan     = [A11 A12; A12   A22];
iMdvp       = [R11 R12; R12   R22]; 
%etaf_k      = etaf_k/dx;
% numerics
nout        = 250;
nt          = 7000;
chi         = 0.5;

A3_m        = (R11 * R22 - R12 ^ 2) * (A11 * A22 - A12 ^ 2); % eq. (34)*(33)
A2_m        = (A11 * R11 - 2 * A12 * R12 + A22 * R22) ;      % eq.(35)
V1_HF       = 1./ (( A2_m - (A2_m.*A2_m - 4.*A3_m)^0.5 )./2./A3_m ).^0.5; % eq.(31)
V2_HF       = 1./ (( A2_m + (A2_m.*A2_m - 4.*A3_m)^0.5 )./2./A3_m ).^0.5; % eq.(32)

CFL         = 0.999;
dt          = dx/ V1_HF  *  CFL;   % stable for chi=0.5
%dt          = dx/ V1_HF  *  1.00001; % (uncomment) not stable for chi=0.5 

% chi         = 0.0; CFL = 0.9999; %CFL= 1.001;% (uncomment) Test of the explicit scheme for the Darcy's flux: CFL= 1.0001 not stable 
% dt          = dx.*(-0.3333333333*dx + 0.3333333333*sqrt(dx^2 + 12.) ) * CFL; % eq.(45)


for it = 1:nt  
   
    stress_xx     = stress_xx   + ( iM_ELan(1,1).*  diff(Vx,1,1)/dx + iM_ELan(1,2).*diff(Qx,1,1)/dx).*dt ;
    Prf           = Prf         -  ( iM_ELan(2,1).*diff(Vx,1,1)/dx  + iM_ELan(2,2).*diff(Qx,1,1)/dx)*dt;
    Qx_old        = Qx; 
    Qx(2:end-1)   = (Qx_old(2:end-1)/dt -  diff(stress_xx,1,1)/dx.*iMdvp(2,1) - (diff(Prf,1,1)/dx +                     (1-chi).*Qx_old(2:end-1) .*etaf_k).*iMdvp(2,2))./(1/dt + chi.*iMdvp(2,2).*etaf_k);       
    Vx(2:end-1)   = (Vx(2:end-1)/dt     +  diff(stress_xx,1,1)/dx.*iMdvp(1,1) + (diff(Prf,1,1)/dx + (chi.*Qx(2:end-1) + (1-chi).*Qx_old(2:end-1)).*etaf_k).*iMdvp(1,2))*dt;      
   
    if mod(it,nout)==1 
        figure(2);
        subplot(4,1,1), plot(x,(Vx(2:end) + Vx(1:end-1))/2),    title('Vx'),
        subplot(4,1,2), plot(x,stress_xx),        title('Stress_{xx}'),
        subplot(4,1,3), plot(x,(Qx(2:end) + Qx(1:end-1))/2),title('Qxf'), 
        subplot(4,1,4), plot(x,Prf),title('Prf'), 
        xlabel(['it = ', num2str(it)]),drawnow
    end
end
