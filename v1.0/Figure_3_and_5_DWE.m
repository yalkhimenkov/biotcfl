%%
% DWE: this script calculates the stability of explicit, implicit-explicit and implicit schemes of the damped wave equation
% Copyright (C) 2020  Yury Alkhimenkov, Lyudmila Khakimova, Yury Podladchikov.
% Matlab R2018a

% This is a free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version. 

% Please cite us if you use our routine: 
% Alkhimenkov Y., Khakimova L., Podladchikov Y.Y., 2020. 
% Numerical stability of discrete schemes of Biot's poroelastic equations. 
% Geophysical Journal International.
%%
close all,figure(1)
clear,clf,colormap(jet), clc; format compact; format longG; 

maxdis = 20;
for dis = 1:maxdis
% parameters with independent units/ independent scales
scale = linspace(1,5,maxdis);
Lx          = 10.^(scale(dis)); % m
tau         = 1e0;
K           = 1e0;  
rho         = 1e0; 
% numerics
nx           = 1001;
dx           = Lx/(nx-1);
x            = (-Lx/2:dx:Lx/2)';
V            = sqrt(K/rho);
% formulas for dt of different numerical schemes
dt_dif_1             = 2 *tau ;              % eq.(A.6) or eq.(23)
D                    = K*tau/rho;  
dt_diffusion_eq(dis) = dx*dx/D / (2);        % eq.(12)
dt_wave_eq(dis)      = dx/(sqrt(K/rho));     % eq.(A.3) or eq.(19)
dt_DWE_expl(dis)     = dx.*( -(dx*rho)+ sqrt( (dx*rho).^2+16*K*rho*tau^2) )./(4*K*tau); % eq.(A.1)  or eq.(16)
dt_DWE_impl(dis)     = dx.*(dx*rho + sqrt(16*K*rho*tau.^2 + dx^2*rho^2))   ./(4*tau*K);      % eq.(A.10) or eq.(26)

% for positive dt
chi = 0;
dt_general_ex(dis)     = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
chi = 0.5;
dt_general_exim(dis)     = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
chi = 1;
dt_general_im(dis)     = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
% for negative dt
chi = 0;
dt_general_ex_n(dis)     = dx.*(-1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
chi = 0.5;
dt_general_exim_n(dis)     = dx.*(-1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
chi = 1;
dt_general_im_n(dis)     = dx.*(-1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)

% validation --- yellow curve (not in the paper).Comment line 69 to disable
chi                  =  1; % chi=1,=> implicit scheme, chi=0.5,=> implicit-exlicit scheme, chi=0,=> explicit scheme,
dt_general(dis)     = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau); % eq.(A.11) or eq.(27)
end

x_ax         = (10.^scale)/(nx-1) * (1/tau);
Lx1          = x_ax(1);Lx2 = x_ax(end);

plot(x_ax, dt_DWE_expl     ,'ro-','LineWidth',4); hold on
plot(x_ax, dt_wave_eq   ,'ko-','LineWidth',4);hold on
plot(x_ax, dt_DWE_impl   ,'bo-','LineWidth',4);hold on
plot(x_ax, dt_wave_eq   ,'g--','LineWidth',2.5);hold on
plot(x_ax, dt_diffusion_eq,'--','color','[0.8500, 0.3250, 0.0980]','LineWidth',2.5);hold on
plot([Lx1  Lx2 ], [dt_dif_1 dt_dif_1],'--','color','[0., 0.8, 0.8]','LineWidth',2.5);hold on

plot(x_ax, dt_general   ,'y--','LineWidth',2.5);hold on % For validation, comment to remove yellow dashed curve

set(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
legend('DWE \chi=0','DWE \chi=1/2','DWE \chi=1','Wave Equation',...
    'Diffusion Equation','Parabolic limit', 'Location','northwest');legend boxoff ;
xlabel('$$\Delta {x} \, (-)$$','interpreter','latex'); ylabel('$$\Delta {t} \, (-)$$','interpreter','latex');%\tilde{t}
ylim([10^(-1) 10^2]);    
xlim([10^(-1) 10^2]); 

%%
figure(2);clf; 
subplot(211);
plot(x_ax,dt_general_ex,'ro-','LineWidth',4); hold on
plot(x_ax, dt_general_exim,'ko-','LineWidth',4);hold on
plot(x_ax, dt_general_im,'bo-','LineWidth',4);hold on
set(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
legend('DWE \chi=0','DWE \chi=1/2','DWE \chi=1', 'Location','northwest');legend boxoff ;
xlabel('$$\Delta {x} \, (-)$$','interpreter','latex'); ylabel('$$\Delta {t} \, (-)$$','interpreter','latex');
subplot(212);
plot(x_ax,dt_general_ex_n,'ro-','LineWidth',4); hold on
plot(x_ax, dt_general_exim_n,'ko-','LineWidth',4);hold on
plot(x_ax, dt_general_im_n,'bo-','LineWidth',4);hold on
set(gca, 'XScale', 'log');set(gca, 'YScale', 'log');
xlabel('$$\Delta {x} \, (-)$$','interpreter','latex'); ylabel('$$\Delta {t} \, (-)$$','interpreter','latex');

%%
if 1>4, return, end  % use 5 instead of 1 to stop here
%%

figure(5);clf; %clf
% parameters with independent units
Lx           = 1e0;    % m
tau          = 0.2e0;  % 
K            = 1e0;    % Bulk modulus
rho          = 1e0;    % Density
%numerics
nx           = 4001;
nout         = 50;
nt           = 4000+1 ;
dx           = Lx/(nx-1);
x            = (-Lx/2:dx:Lx/2)';
% dependent units
V            = sqrt(K/rho);
lamx         = 1/25*Lx;
% initial conditions
Vx           = zeros(nx+1,1);
Qx           = zeros(nx+1,1);
Prt          = 0.00*exp(-(x/lamx).^2);
Prf          = 1*exp(-(x/lamx).^2);
% dt validation
chi          = 0.0; % chi=1,=> implicit scheme, chi=0.5,=> implicit-exlicit scheme, chi=0,=> explicit scheme,
dt_general   = dx.*(1/V*sqrt(4*(chi - 1/2)^2*dx^2*1/V^2 + 16*tau^2) + 2*(chi - 1/2)*1/V^2*dx)/(4*tau);  % eq.(A.11) or eq.(27)
dt           = dt_general*0.999;

for it = 1:nt
    
    Prf           = Prf              -       K.*diff(Qx,1,1) /dx*dt;
    Qx(2:end-1)   = (Qx(2:end-1)/dt  - (1./rho.*diff(Prf,1,1)/dx + (1-chi).*Qx(2:end-1) ./tau))./(1/dt + chi./tau);
    
    if mod(it,nout)==1
        figure(5);
        subplot(2,1,1), plot(x,(Qx(2:end) + Qx(1:end-1))/2,'r','LineWidth',2),title('Qxf')       
        subplot(2,1,2), plot(x,Prf,'r','LineWidth',2),title('Prf'), 
        count = it*1;
        xlabel(['  it = ', num2str(it)]),drawnow
    end
end


